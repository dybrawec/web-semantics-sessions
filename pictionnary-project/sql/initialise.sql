CREATE DATABASE pictionnary CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'pictionnary_user'@'localhost' IDENTIFIED BY  'pictionnary_user';  
-- GRANT USAGE ON * . * TO  'pictionnary_user'@'localhost' IDENTIFIED BY  'pictionnary_user';  
GRANT ALL PRIVILEGES ON  `pictionnary` . * TO  'pictionnary_user'@'localhost';  
