# Reponses du séance 28-11-17
__[Page web du séance](http://software.dybrawa.com/iut/web-semantics/sessions/28-11-17/)__

### C'est quoi les attributs action et method ?
_Action_ est la page à laquelle la requête sera envoyée. _Method_ spécifie le mode d'envoie de la requête.

### Qu'y a-t-il d'autre comme possiblité que post pour l'attribut method ?
_Post_ et _get_.

### Quelle est la différence entre les attributs name et id ?
L'attribut _id_ est utilisé pour avoir une référence de l'élement dans CSS / JS.
L'attribut _name_ est utilisé pour identifier les données dans la requête envoyé, issue du formulaire.

### C'est lequel qui doit être égal à l'attribut for du label ?
L'attribut _name_.

### Quels sont les deux scénarios où l'attribut title sera affiché ?
Si l'utilisateur essaie de valider pendant que le contenu n'est pas conforme au regex.

### Quelle est la différence entre les attributs name et id ?
L'attribut _id_ est utilisé pour avoir une référence de l'élement dans CSS / JS.
L'attribut _name_ est utilisé pour identifier les données dans la requête envoyé, issue du formulaire.

### Pourquoi est-ce qu'on a pas mis un attribut name ici ?
Parce que nous n'en aurons pas besoin pour la requête, ce champ est utilisé juste pour la vérficiation avec js.

### Quel scénario justifie qu'on ait ajouté l'écouter validateMdp2() à l'évènement onkeyup de l'input mdp1 ?
Scénario d'inégalité entre les deux champs.

### La vérification de l'égalité des mots de passe est effectuée directement avec l'API validation, lors d'un keyup sur l'un des deux champs. Pourquoi sur les deux champs ?
Car ils sont du même type, _password_.

