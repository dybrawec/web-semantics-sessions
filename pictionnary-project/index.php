<!DOCTYPE html>  
<html>  
	<head>  
		<meta charset=utf-8 />  
		<title>Pictionnary - Inscription</title>  
		<link rel="stylesheet" media="screen" href="css/styles.css" >
	</head>  
	<body>  

		<h2>Inscrivez-vous</h2>  
		<form class="inscription" action="req_inscription.php" method="post" name="inscription">  
			<span class="required_notification">Les champs obligatoires sont indiqués par *</span>  
			<ul>  
				<li>  
					<label for="email">E-mail :</label>  
					<input type="email" name="email" id="email" autofocus required/>  
					<span class="form_hint">Format attendu "name@something.com"</span>  
				</li>  
				<li>  
					<label for="prenom">Prénom :</label>  
					<input type="text" name="prenom" id="prenom" placeholder="prenom" required/>  
				</li>  
				<li>  
					<label for="mdp1">Mot de passe :</label>  
					<input type="password" name="password" id="mdp1" pattern="[A-Za-z0-9]{6,8}" title = "Le mot de passe doit contenir de 6 à 8 caractères alphanumériques." placeholder="mot de passe" required>  
					<span class="form_hint">De 6 à 8 caractères alphanumériques.</span>  
				</li>  
				<li>  
					<label for="mdp2">Confirmez mot de passe :</label>  
					<input type="password" id="mdp2" required onkeyup="validateMdp2()" placeholder="mot de passe" required>  
					<span class="form_hint">Les mots de passes doivent être égaux.</span>  
					<script>  
					validateMdp2 = function(e) {  
						var mdp1 = document.getElementById('mdp1');  
						var mdp2 = document.getElementById('mdp2');  
						if (mdp1.checkValidity() && mdp1.value == mdp2.value) {  
							// ici on supprime le message d'erreur personnalisé, et du coup mdp2 devient valide.  
							document.getElementById('mdp2').setCustomValidity('');  
						} else {  
							// ici on ajoute un message d'erreur personnalisé, et du coup mdp2 devient invalide.  
							document.getElementById('mdp2').setCustomValidity('Les mots de passes doivent être égaux.');  
						}  
					}
					</script>
				</li>
				<li>  
					<label for="birthdate">Date de naissance:</label>  
					<input type="date" name="birthdate" id="birthdate" placeholder="JJ/MM/AAAA" required onchange="computeAge()"/>  
					<script>  
					computeAge = function(e) {  
						try{
							var difference = new Date( Date.now() ).getYear();
							difference -= new Date(document.getElementById( "birthdate" ).value).getYear();
							document.getElementById( "age" ).value = difference;
						} catch(e) {  
							document.getElementById( "age" ).value = "";
						}  
					}  
					</script>  
					<span class="form_hint">Format attendu "JJ/MM/AAAA"</span>  
				</li>  
				<li>  
					<label for="age">Age:</label>  
					<input type="number" name="age" id="age" disabled/>  
				</li>
			        <li>  
					<label for="profilepicfile">Photo de profil:</label>  
					<input type="file" id="profilepicfile" onchange="loadProfilePic(this)"/>  
					<span class="form_hint">Choisissez une image.</span>  
					<input type="hidden" name="profilepic" id="profilepic"/>  
					<canvas id="preview" width="0" height="0"></canvas>  
					<script>  
					    loadProfilePic = function (e) {  
						var canvas = document.getElementById("preview");  
						var ctx = canvas.getContext("2d");  
						ctx.fillRect(0,0,canvas.width,canvas.height);  
						canvas.width=0;  
						canvas.height=0;  
						var file = document.getElementById("profilepicfile").files[0];  
						var img = document.createElement("img");  
						var reader = new FileReader();  
						reader.onload = function(e) {  
						    if (!file.type.match(/image.*/)) {  
							document.getElementById("profilepicfile").setCustomValidity("Il faut télécharger une image.");  
							document.getElementById("profilepicfile").value = "";  
						    }  
						    else {  
							img.src = e.target.result;  
							img.onload = function() {
								document.getElementById("profilepicfile").setCustomValidity("");  
								var MAX_WIDTH = 96;  
								var MAX_HEIGHT = 96;  
								var width = img.width;  
								var height = img.height;  
				      
								if( width > MAX_WIDTH ) {
									height *= ( MAX_WIDTH / width );
									width = MAX_WIDTH;
								}

								if( height > MAX_HEIGHT ) {
									width *= ( MAX_HEIGHT / height );
									height = MAX_HEIGHT;
								}

								canvas.width = width;  
								canvas.height = height;  
								ctx.drawImage(img, 0, 0, width, height);  
								var dataurl = canvas.toDataURL("image/png");  
								document.getElementById("profilepic").value = dataurl;  
							}
						    };  
						}  
						// on charge l'image pour de vrai, lorsque ce sera terminé le callback loadProfilePic sera appelé.  
						reader.readAsDataURL(file);  
					    }  
					</script>  
				</li>  
				<li>
					<input type="submit" value="Soumettre Formulaire">  
				</li> 
			</ul>  
		</form>  
	</body>  
</html> 
