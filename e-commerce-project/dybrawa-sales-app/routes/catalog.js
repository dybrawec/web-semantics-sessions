module.exports = function (router) {
    var category = require('../controllers/catalog.js');

    router.get('/', category.index);

    router.get('/', category.index);

    exports.index = function (req, res) {
        res.render('pages/front/catalog/index');
    }

    exports.albums = function (req, res) {
        // get a reasonable amount of albums and feature a few songs / cards

        res.render('pages/front/catalog/albums');
    }

    exports.album = function (req, res) {
        req.params.album_sku;

        // get songs corresponding to album_sku and page

        res.render('pages/front/catalog/album');
    }

    exports.song = function (req, res) {
        req.params.song_sku;

        // get song corresponding to song_sku

        res.render('pages/front/catalog/song');
    }
}