module.exports = function (router) {
    var admin = require('../controllers/admin');

    router.get('/', admin.sign_in);

    router.post('/', admin.activate_admin);

    router.get('/dashboard', admin.dashboard);

    router.get('/manage/orders', admin.manage_orders);

    router.post('/manage/orders/delete', admin.manage_orders_delete);

    router.get('/manage/orders/modify', admin.manage_orders_modify);

    router.post('/manage/orders/modify', admin.manage_orders_update);

    router.get('/manage/albums', admin.manage_albums);

    router.get('/manage/albums/create', admin.manage_albums_create);

    router.post('/manage/albums/create', admin.manage_albums_add);

    router.post('/manage/albums/delete', admin.manage_albums_delete);

    router.get('/manage/albums/modify', admin.manage_albums_modify);

    router.post('/manage/albums/modify', admin.manage_albums_update);

    router.get('/manage/songs', admin.manage_songs);

    router.get('/manage/songs/create', admin.manage_songs_create);

    router.get('/manage/songs/create', admin.manage_songs_add);

    router.post('/manage/songs/delete', admin.manage_songs_delete);

    router.get('/manage/songs/modify', admin.manage_songs_modify);

    router.post('/manage/songs/modify', admin.manage_songs_update);

    router.get('/manage/users', admin.manage_users);

    router.post('/manage/users/delete', admin.manage_users_delete);

    router.get('/manage/users/modify', admin.manage_users_modify);

    router.post('/manage/users/modify', admin.manage_users_update);

    router.get('/sign_out', admin.sign_out);
}