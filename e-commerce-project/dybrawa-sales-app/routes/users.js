module.exports = function (router) {
    var users = require('../controllers/users');

    router.get('/', users.index);

    router.get('/settings', users.settings);

    router.post('/update', users.update);

    router.post('/password', users.password);

    router.get('/sign_in', users.sign_in);

    router.post('/sign_in', users.activate_user);

    router.get('/sign_up', users.sign_up);

    router.post('/sign_up', users.create_user);

    router.get('/dashboard', users.dashboard);

    router.get('/sign_out', users.sign_out);
    
    router.get('/delete', users.delete);
}
