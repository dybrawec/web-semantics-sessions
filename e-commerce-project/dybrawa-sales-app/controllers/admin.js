var Admin = require('../models/admin');
var User = require('../models/users');
//var Order = require('../models/order');
var Album = require('../models/album');
var Song = require('../models/song');
var encrypt = require('../bin/encrypt');

exports.sign_in = function (req, res) {
    if (req.session.admin_email)
        return res.redirect('/admin/dashboard');

    res.render('pages/admin/sign_in');
}

exports.activate_admin = function (req, res) {
    //return res.end(encrypt(req.body.password));

    if (!req.body.admin_email || !req.body.password)
        return res.redirect('/admin');

    Admin.findById(req.body.admin_email).then(admin => {
        if (!admin || admin.password != encrypt(req.body.password))
            return res.redirect('/admin?error=bad_credentials');

        req.session.admin_email = admin.email;

        return res.redirect('/admin/dashboard');
    });
}

exports.dashboard = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    res.render('pages/admin/dashboard');
}

exports.manage_orders = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    res.render('pages/admin/manage/orders');
}

exports.manage_orders_delete = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

}

exports.manage_orders_modify = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

}

exports.manage_orders_update = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

}

exports.manage_albums = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    Album.findAll().then(result => {
        res.render('pages/admin/manage/albums', {
            albums: result
        });
    });
}

exports.manage_albums_create = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    res.render('pages/admin/manage/albums/create');
}

exports.manage_albums_add = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    if (!req.body.sku || !req.body.name || !req.body.description || !req.body.art)
        return res.redirect('/admin/manage/albums/create');

    Album.findById(req.body.sku)
        .then(album => {
            if (album)
                return res.redirect('/admin/manage/albums/create?error=existing');

            Album.create({ sku: req.body.sku, name: req.body.name, description: req.body.description, art: req.body.art })
                .then(album_created => {
                    res.redirect('/admin/manage/albums?success=create');
                });
        });
}

exports.manage_albums_delete = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    if (!req.body.skuToDelete)
        return res.redirect('/admin/manage/albums');

    return Album.findById(req.body.skuToDelete).then(album => {
        album.destroy().then(function (deleted) {
            if (deleted)
                return res.redirect('/admin/manage/albums?success=deleted')
            else
                return res.redirect('/admin/manage/albums?error=deleted')
        });
    });

}

exports.manage_albums_modify = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    if (!req.query.skuToModify)
        return res.redirect('/admin/manage/albums');

    return Album.findById(req.query.skuToModify).then(album => {
        res.render('pages/admin/manage/albums/modify', {
            album: album
        });
    });

}

exports.manage_albums_update = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    if (!req.body.sku)
        return res.redirect('/admin/manage/albums/');

    return Album.findById(req.body.sku).then(album => {
        album.update({
            name: req.body.name,
            description: req.body.description,
            art: req.body.art
        }).then(() => {
            return res.redirect('/admin/manage/albums?success=update');
        });
    });
}

exports.manage_songs = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    Song.findAll().then(result => {
        res.render('pages/admin/manage/songs/', {
            songs: result
        });
    });
}

exports.manage_songs_create = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

}

exports.manage_songs_add = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

}

exports.manage_songs_delete = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

}

exports.manage_songs_modify = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

}

exports.manage_songs_update = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

}

exports.manage_users = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    User.findAll().then(result => {
        res.render('pages/admin/manage/users', {
            users: result
        });
    });
}

exports.manage_users_delete = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    if (!req.body.emailToDelete)
        return res.redirect('/admin/manage/users');

    return User.findById(req.body.emailToDelete).then(user => {
        user.destroy().then(function (deleted) {
            if (deleted)
                return res.redirect('/admin/manage/users?success=deleted')
            else
                return res.redirect('/admin/manage/users?error=deleted')
        });
    });
}

exports.manage_users_modify = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    if (!req.query.emailToModify)
        return res.redirect('/admin/manage/users');

    return User.findById(req.query.emailToModify).then(user => {
        res.render('pages/admin/manage/users/modify', {
            user: user
        });
    });
}

exports.manage_users_update = function (req, res) {
    if (!req.session.admin_email)
        return res.redirect('/admin');

    if (!req.body.email)
        return res.redirect('/admin/manage/users');

    return User.findById(req.body.email).then(user => {
        user.update({
            name: req.body.name
        }).then(() => {
            return res.redirect('/admin/manage/users?success=update');
        });
    });
}

exports.sign_out = function (req, res) {
    delete req.session.admin_email;

    /*req.session.destroy(function (err) {

    });*/

    return res.redirect('/admin');
}