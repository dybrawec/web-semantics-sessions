var User = require('../models/users');
var encrypt = require('../bin/encrypt');

exports.index = function (req, res) {
    return res.redirect('/users/dashboard');
};

exports.settings = function (req, res) {
    if (!req.session.email)
        return res.redirect('/users/sign_in');

    User.findById(req.session.email).then(user => {
        res.render('pages/front/users/settings', {
            name: user.name
        });
    });
}

exports.update = function (req, res) {
    if (!req.session.email)
        return res.redirect('/users/sign_in');

    if (!req.body.name)
        return res.redirect('/users/settings')

    return User.findById(req.session.email).then(user => {
        user.update({
            name: req.body.name
        }).then(() => {
            return res.redirect('/users/settings?success=update');
        });
    });
}

exports.password = function (req, res) {
    if (!req.session.email)
        return res.redirect('/users/sign_in');

    if (!req.body.old_password || !req.body.new_password || !req.body.confirm_password)
        return res.redirect('/users/settings')

    if (req.body.new_password != req.body.confirm_password)
        return res.redirect('/users/settings?error=password_match');

    return User.findById(req.session.email).then(user => {
        if (!user || user.password != encrypt(req.body.old_password))
            return res.redirect('/users/settings?error=password_old');

        user.update({
            password: req.body.new_password
        }).then(() => {
            return res.redirect('/users/settings?success=password');
        });
    });
}

exports.sign_in = function (req, res) {
    if (req.session.email)
        return res.redirect('/users/dashboard');
    res.render('pages/front/users/sign_in');
}

exports.activate_user = function (req, res) {
    if (!req.body.email || !req.body.password)
        return res.redirect('/users/sign_in');

    User.findById(req.body.email).then(user => {
        if (!user || user.password != encrypt(req.body.password))
            return res.redirect('/users/sign_in?error=bad_credentials');

        req.session.email = user.email;

        return res.redirect('/users/dashboard');
    });
}

exports.sign_up = function (req, res) {
    if (req.session.email)
        return res.redirect('/users/dashboard');
    res.render('pages/front/users/sign_up');
}

exports.create_user = function (req, res) {
    if (!req.body.email || !req.body.password || !req.body.name)
        return res.redirect('/users/sign_up');

    User.findById(req.body.email)
        .then(user => {
            if (user)
                return res.redirect('/users/sign_up?error=existing_user');

            User.create({ email: req.body.email, name: req.body.name, password: req.body.password })
                .then(user_created => {
                    res.redirect('/users/sign_in?success=true');
                });
        });

}

exports.dashboard = function (req, res) {
    if (!req.session.email)
        return res.redirect('/users/sign_in');

    return User.findById(req.session.email).then(foundUser => {
        res.render('pages/front/users/dashboard', {
            user: foundUser
        });
    });
};

exports.sign_out = function (req, res) {
    delete req.session.email;

    /*req.session.destroy(function (err) {

    });*/

    return res.redirect('/');
}

exports.delete = function (req, res) {
    if (!req.session.email)
        return res.redirect('/users/sign_in');

    return User.findById(req.session.email).then(user => {
        user.destroy().then(() => {
            res.redirect('/users/sign_out');
        }); 
    });
}
