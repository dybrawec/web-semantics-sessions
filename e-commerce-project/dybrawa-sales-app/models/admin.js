
var db = require('../bin/db');
var Sequelize = require('sequelize');
var encrypt = require('../bin/encrypt.js');

const Admin = db.define('admin', {
    email: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
        set(val) {
            this.setDataValue('password', encrypt(val));
        }
    },
});

Admin.sync();

module.exports = Admin;
