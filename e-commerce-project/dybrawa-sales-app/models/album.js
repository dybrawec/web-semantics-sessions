
var db = require('../bin/db');
var Sequelize = require('sequelize');

const Album = db.define('album', {
    sku: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT
    },
    art: {
        type: Sequelize.STRING,
        allowNull: false
    },
});

Album.sync();

module.exports = Album;
