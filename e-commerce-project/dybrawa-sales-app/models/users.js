var db = require('../bin/db');
var Sequelize = require('sequelize');
var encrypt = require('../bin/encrypt.js');

const User = db.define('users', {
    email: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
        set(val) {
            this.setDataValue('password', encrypt(val));
        }
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
});

User.sync();

module.exports = User;
