var db = require('../bin/db');
var Sequelize = require('sequelize');

var Album = require('./album');

const Song = db.define('song', {
    sku: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT
    },
    type: {
        type: Sequelize.STRING,
        allowNull: false
    },
    price: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: false
    },
    link: {
        type: Sequelize.STRING,
        alloNull: false
    },
    art: {
        type: Sequelize.STRING,
        allowNull: false
    },
});

Song.belongsTo(Album);

Song.sync();

module.exports = Song;
