var express = require('express');
var enrouten = require('express-enrouten');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var ejsLocals = require('ejs-locals');

var app = express();

app.engine('ejs', ejsLocals)

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));

app.use(session({
    secret: 'keyboard cat',
    cookie: {}
}));

app.use(function (req, res, next) {
    res.locals._email = req.session.email;
    res.locals._admin_email = req.session.admin_email;

    res.locals._error = req.query.error;
    res.locals._success = req.query.success;

    next();
});

app.use(enrouten({
    directory: 'routes'
}));

app.use('/javascript', express.static(__dirname + '/node_modules/bootstrap/dist/js')); // redirect bootstrap JS
app.use('/stylesheets', express.static(__dirname + '/node_modules/bootstrap/dist/css')); // redirect CSS bootstrap

var server = app.listen(80, () => {
    var host = server.address().address;
    var port = server.address().port;

    console.log("App listening at http://%s:%s", host, port);
});
